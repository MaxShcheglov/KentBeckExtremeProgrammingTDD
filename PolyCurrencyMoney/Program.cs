﻿using System;
using System.Collections;

namespace PolyCurrencyMoney
{
    class Program
    {
        static void Main(string[] args)
        {
        }
    }

    public class Money : IExpression
    {
        protected int amount;
        protected string currency;

        public int Amount { get => amount; }

        public Money(int amount, string currency)
        {
            this.amount = amount;
            this.currency = currency;
        }

        public override bool Equals(object obj)
        {
            Money money = (Money)obj;
            return amount == money.Amount && Currency().Equals(money.Currency());
        }

        public static Money Dollar(int amount)
        {
            return new Money(amount, "USD");
        }

        public static Money Franc(int amount)
        {
            return new Money(amount, "CHF");
        }

        public IExpression Times(int multiplier)
        {
            return new Money(amount * multiplier, currency);
        }

        public IExpression Plus(IExpression addend)
        {
            return new Sum(this, addend);
        }

        public string Currency()
        {
            return currency;
        }

        public Money Reduce(Bank bank, string to)
        {
            int rate = bank.Rate(currency, to);
            return new Money(amount / rate, to);
        }
    }

    public interface IExpression
    {
        Money Reduce(Bank bank, string to);
        IExpression Plus(IExpression tenFrancs);
        IExpression Times(int multiplier);
    }

    public class Bank
    {
        private Hashtable rates = new Hashtable();

        public int Rate(string from, string to)
        {
            if (from.Equals(to)) return 1;
            int rate = (int)rates[new Pair(from, to)];
            return rate;
        }

        public Money Reduce(IExpression source, string to)
        {
            return source.Reduce(this, to);
        }

        public void AddRate(string from, string to, int rate)
        {
            rates.Add(new Pair(from, to), rate);
        }
    }

    public class Sum : IExpression
    {
        private IExpression augend;
        private IExpression addend;

        public Sum(IExpression augend, IExpression addend)
        {
            this.augend = augend;
            this.addend = addend;
        }

        public IExpression Augend { get => augend; }

        public IExpression Addend { get => addend; }

        public IExpression Plus(IExpression addend)
        {
            return new Sum(this, addend);
        }

        public Money Reduce(Bank bank, string to)
        {
            int amount = augend.Reduce(bank, to).Amount + addend.Reduce(bank, to).Amount;
            return new Money(amount, to);
        }

        public IExpression Times(int multiplier)
        {
            return new Sum(augend.Times(multiplier), addend.Times(multiplier));
        }
    }

    public class Pair
    {
        private string from;
        private string to;

        public Pair(string from, string to)
        {
            this.from = from;
            this.to = to;
        }

        public override bool Equals(object obj)
        {
            Pair pair = (Pair)obj;
            return from.Equals(pair.from) && to.Equals(pair.to);
        }

        public override int GetHashCode()
        {
            return 0;
        }
    }
}
